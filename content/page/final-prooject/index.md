---
title: Final Project
date:
layout:
slug:
image:
aliases:
  - about-us
  - about-hugo
  - contact

menu:
    main:
        weight: -70
        params:
            icon: archives
---

## Inspirations and sketch


### My first idea is to play with light caustics.

"Caustics may be defined as the envelope of light rays that have been reflected or refracted from a curved surface and projected onto a surface where they can be visualised."

![Water drop and cause light caustics](notion_motion2.jpg)
![Olafur eliasson notion motion](notion_motion.jpg)
![The Ripple Light by poetic Lab](ripple-light.jpg)

The light dancing and waving always draws my attention. Light passing layers of leaves, traveling through shallow water, and wiggling on the sand, brings me peace and I can just stare for a long time. Thus, I came up with an idea about making a wall-mounted lamp and using the wall as a caustic canvas.

![light caustics](first_sketch1.jpg)

The first and second sketches use a small stick. When the sensor detects movement, it sends the signal to a motor, which actuates a stick to hit the surface of the liquid, and makes the light dance.

The third drawing uses a denser medium panel with a patterned surface, such as glass or acrylic. And the signal triggers the panel to rotate, to cause caustic waves.  


### The second idea uses an array of LEDs
![light art](first_sketch2.jpg)
In the first sketch. LED Lights are placed around the circle and the signal tells them to switch on and off.

The second sketch shows an array of LEDs that are placed on different rails on the frame. A motor controls the lights to move left and right, up and down, to create different light images every time.

Then later, I also come up with another idea of adding clock hands to the light frame and making it into an interactive light clock.
