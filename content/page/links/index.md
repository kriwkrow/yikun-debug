---
title: Useful Links
links:
  - title: Markdown cheatsheet
    description: Markdown basic and extended syntax
    website: https://www.markdownguide.org/cheat-sheet/
    image: markdown.JPG

  - title: HTML Tags
    description: HTML Tags cheatsheet
    website: https://www.w3schools.com/tags/default.asp
    image:

  - title: Hugo
    description: Introduction to Hugo Templating
    website: https://gohugo.io/templates/introduction/
    image: hugo.JPG

  - title: Hugo - Static Site Generator Tutorial
    description: By Giraffe Academy
    website: https://www.youtube.com/playlist?list=PLLAZ4kZ9dFpOnyRlyS-liKL5ReHDcj4G3
    image:


menu:
    main:
        weight: -50
        params:
            icon: link

comments: false
---
