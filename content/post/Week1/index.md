---
title: Week01. Introduction
description: Learning about Git, HTML basic and CSS
date:
slug:
image: head.PNG
categories:
---

## Goal for the week


* Studying HTML basics
* Studying css
* Git setup
* build a simple website with html and css
* Plan and sketch a potential final project


## HTML basics
Building a website is quite new to me. But the HTML Basics tutorial by Kris was quite detailed and shows step by step how to create a plain website with basic tags.

### understanding basic tags   
Here is my notes of what I learned from tutorial：

{{< highlight html >}}
Heading: <!--h1 h2 h3 h4 h5 h6-->
<h1>title</h1>

Paragraph: <p>Text</p>   
<b>bold</b>, <strong>bold text</strong>, <i>italicized</i>, <u>underlined</u>

image:
<img src="image.jpg">

lists:
<ol> <!--organized Lists-->
    <li>Element one</li>
    <li>Element two</li>
</ol>

<ul> <!-unorganized Lists>
    <li>First</li>
    <li>Second</li>
</ul>

tables:   <!--rows:<tr> ; columns:<td>-->
<table>
    <tr>
        <td>This column 1</td>
        <td>This column 2</td>
    </tr>
</table>

Hyperlinks and the Menu:
<a href="index.html">index</a>

{{< /highlight >}}


A few other tags I learned from other material:
{{< highlight html >}}
<br>
<div class
{{< /highlight >}}

### building structure for the website
{{< highlight html >}}
<!doctype html>
<html>
<head>
  <title>Yikun Fab Academy</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width">
</head>

<body>
  <h1>title/<h1>
  <p>Text</p>
  <img src="Me.jpg" width="100%">
</body>

</html>
{{< /highlight >}}

From this part, I am capable of building the most basic documentation website.


### My review of HTML

During the building of my website, I found out the code can be quite long with simple contents. For example, I would like to show some of my previous works with the same format. They use the same structure like is:

{{< highlight html >}}
<h2>Mirror Chair</h2>
<table width="80%" cellspacing="5" cellpadding="5">
  <tr>
    <td>
      <img src="mirror_chair.jpg" width="100%">
      <figcaption>Those chairs use 21mm and 9mm birch plywood. Designed to be disassembled and mirrorable. They were fabricated by CNC milling and assembled by hand.</figcaption>
    </td>
  </tr>
</table>
{{< /highlight >}}

It does not look complicated, but when it repeat five times, it is getting hard to modify.

Another thing is that text can be hard to modify as they does not return and become quite long. Every time I would like to fix something, I need to drag very far just to change one word.



## Choosing Text editor
I tried three different text editor:
* Atom
* Visual Studio code
* Bracket

They all have their benefits:

**VS code** has pretty great auto fill with tags.  
**Bracket** allows live preview and changing color in a very easy way.  
Then I realize the power of **Atom** --- the package. It can have all the great benefits the other two editor have, and even more by installing package. I installed color-picker and atom-html-preview, and they are quite useful!


## CSS Studying and modify my website
For CSS learning, I started with the [HTML CSS Lecture of Fab Academy 2015](https://www.youtube.com/watch?v=PeWIU7UQ_rc&ab_channel=openp2pdesign)

It is a great lecture, I tried as much as possible what Massimo introduced to modify my website

### font
Load [google external font](https://fonts.google.com/),  
Add fond link to <head> of html
copy css rules to css file

change specific text ---> add id


## Git study and repository upload
I am quite new to terminal. I was a bit lost in the beginning, but soon I caught up by watching tutorials and able to run some simple command.

### Git setup
* Install [Git](https://gitforwindows.org/)
Remember select atom as the text editor when install.
* Configure Git
git config –-global user.name “YOUR_USERNAME”  
git config -–global user.email “jSmith@mail.com”
### [Use SSH keys to communicate with GitLab](https://docs.gitlab.com/ee/ssh/)
* generate SSH key  
ssh-keygen -t ed25519 -C "<<comment>>"  
Accept the suggested filename and directory  
checking SSH key
* add SSH keyto gitlab    
in terminal:  
cat ~/.ssh/id_ed25519.pub | clip  
in gitlab:  
Avatar -- Preferences -- SSH Keys -- paste public key to key box --- type a description in title box (worklaptop) ---add key
* clone repository to local  
git clone

### upload repository online


*  work-flow  
git status  
git Add  
git commit -m "descriptive commit message"  
git push  
![using git to update my gitlab repository](git_push.PNG)

refresh project in gitlab  
check pipe status ---passed
* other  
git pull  
git merge   
git rm ‘filename’  
git mv ‘filename’ ‘folderName/filename’  
git mv ‘filename’ ‘filename’ <!--rename-->  
git log

## Showcase: My website built with HTML and CSS
![My First simple website with style](first_site.JPG)
